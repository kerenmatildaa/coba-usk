<?php

namespace Database\Seeders;

use App\Models\Barang;
use App\Models\Role;
use App\Models\Saldo;
use App\Models\Transaksi;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class FirstSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(["name" => "Administrator"]);
        $bank_mini = Role::create(["name" => "Bank Mini"]);
        $kantin = Role::create(["name" => "Kantin"]);
        $siswa = Role:: create(["name" => "Siswa"]);

        User::create([
            "name" => "jaehyun",
            "email" => "jaehyun@gmail.com",
            "password" => Hash::make("jaehyun"),
            "role_id" => $admin->id
        ]);

        User::create([
            "name" => "jeno",
            "email" => "jeno@gmail.com",
            "password" => Hash::make("jeno"),
            "role_id" => $bank_mini->id
        ]);

        User::create([
            "name" => "jay",
            "email" => "jay@gmail.com",
            "password" => Hash::make("jay"),
            "role_id" => $kantin->id
        ]);

        $jaehyuk = User::create([
            "name" => "jaehyuk",
            "email" => "jaehyuk@gmail.com",
            "password" => Hash::make("jaehyuk"),
            "role_id" => $siswa->id
        ]);

        Barang::create([
            "name" => "pulpen",
            "image" => "pulpen.jpg",
            "price" => 3000,
            "stock" => 20,
            "desc" => "pulpen tinta hitam"
        ]);

        Saldo::create([
            "user_id" => $jaehyuk->id,
            "saldo" => 50000
        ]);

        Transaksi::create([
            "user_id" => $jaehyuk->id,
            "barang_id" => null,
            "quantity" => 3000,
            "invoice_id" => null,
            "type" => 1,
            "status" => 3
        ]);
    }
}
